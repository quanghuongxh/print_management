<?php

namespace Database\Factories;

use App\Models\Model;
use App\Models\Transaction;
use Illuminate\Database\Eloquent\Factories\Factory;

class TransactionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Transaction::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'code'      => $this->faker->unique()->uuid(),
            'value'     => rand(-1000, 1000) * 1000,
            'source'    => 'Đơn hàng: #'. $this->faker->swiftBicNumber(),
            'note'      => $this->faker->text(),
        ];
    }
}
