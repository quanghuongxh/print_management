<?php

namespace Database\Factories;

use App\Models\Customer;
use Illuminate\Database\Eloquent\Factories\Factory;

class CustomerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Customer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $type = rand(0,1);
        $company = '';
        $tax_code = '';
        
        if ($type) {
            $company = $this->faker->company();
            $tax_code = $this->faker->ean13();
        }

        return [
            'name'      => $this->faker->name(),
            'code'      => $this->faker->unique()->uuid(),
            'status'    => rand(0,1),
            'customer_group_id'  => 1,
            'hotline'   => $this->faker->phoneNumber(),
            'phone'     => $this->faker->phoneNumber(),
            'email'     => $this->faker->email(),
            'zalo'      => $this->faker->phoneNumber(),
            'address'   => $this->faker->address(),
            'type'      => $type,
            'company'   => $company,
            'tax_code'  => $tax_code,
            'description'   => $this->faker->text(),
            'created_at'    => $this->faker->dateTimeBetween('-20 days', now()),
        ];
    }
}
