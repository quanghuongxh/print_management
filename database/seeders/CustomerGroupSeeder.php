<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CustomerGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('xh_customer_group')->insert([
                ['code' => 'NOTGROUP', 'name' => 'Chưa Phân Loại', 'description' => 'Nhóm mặc định'],
        ]);
    }
}
