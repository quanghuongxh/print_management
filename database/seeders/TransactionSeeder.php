<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Transaction;
use App\Models\Customer;

class TransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $transaction = Transaction::factory()
                    ->count(5)
                    ->for(Customer::factory()->state([
                        'name' => 'Đinh Văn Vạn'
                    ]))
                    ->create();
                        
        $transaction = Transaction::factory()
                    ->count(6)
                    ->for(Customer::factory()->state([
                        'name' => 'Đinh Văn Vĩnh',
                    ]))
                    ->create();

        $transaction = Transaction::factory()
                    ->count(7)
                    ->for(Customer::factory()->state([
                        'name' => 'Đinh Công Sơn',
                    ]))
                    ->create();
    }
}
