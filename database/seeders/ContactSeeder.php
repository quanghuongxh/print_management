<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Contact;
use App\Models\Customer;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contact_1 = Contact::factory()
                    ->count(2)
                    ->for(Customer::factory()->state([
                        'id' => 15,
                    ]))
                    ->create();
        $contact_2 = Contact::factory()
                    ->count(3)
                    ->for(Customer::factory()->state([
                        'id' => 14
                    ]))
                    ->create();
        $contact_3 = Contact::factory()
                    ->count(1)
                    ->for(Customer::factory()->state([
                        id => 13
                    ]))
                    ->create();
                    
    }
}
