<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateXhTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xh_transactions', function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable();
            $table->unsignedBigInteger('customer_id');
            $table->decimal('value')->nullable();
            $table->string('source')->nullable();
            $table->string('note')->nullable();
            $table->string('staff')->nullable();
            $table->timestamps();
            $table->foreign('customer_id')->references('id')->on('xh_customers')
                                        ->onUpdate('cascade')
                                        ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xh_transactions');
    }
}
