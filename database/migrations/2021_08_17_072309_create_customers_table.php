<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xh_customers', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('code')->nullable();
            $table->string('status')->default(1);
            $table->unsignedBigInteger('customer_group_id')->default(1);
            $table->string('hotline')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('zalo')->nullable();
            $table->string('address')->nullable();
            $table->tinyInteger('type')->default(1); //ca nhan or cong ty
            $table->string('company')->nullable();
            $table->string('tax_code')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();

            $table->foreign('customer_group_id')->references('id')->on('xh_customer_group')
                                        ->onDelete('set default');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xh_customers');
    }
}
