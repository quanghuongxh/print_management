<?php

use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\CustomerController;
use App\Http\Controllers\Backend\CustomerGroupController;
use App\Http\Controllers\Backend\ContactController;
use Tabuna\Breadcrumbs\Trail;

// All route names are prefixed with 'admin.'.
Route::redirect('/', '/admin/dashboard', 301);
Route::get('dashboard', [DashboardController::class, 'index'])
    ->name('dashboard')
    ->breadcrumbs(function (Trail $trail) {
        $trail->push(__('Home'), route('admin.dashboard'));
    });


// Customer routes admin.customer
Route::resource('customer', CustomerController::class);

//Customer group routes
Route::resource('customer-group', CustomerGroupController::class);

//transaction api
//init load
Route::get('/customer/transaction/get', [
    CustomerController::class, 'loadTransaction'
])->name('customer.transaction');

// Customer Contact
Route::get('/contact/{customer}/create', [
    ContactController::class, 'create'
])->name('contact.create');

Route::post('/contact/store', [
    ContactController::class, 'store'
])->name('contact.store');

Route::get('/contact/{contact}', [
    ContactController::class, 'edit'
])->name('contact.edit');

Route::put('/contact/{contact}', [
    ContactController::class, 'update'
])->name('contact.update');

Route::delete('/contact/destroy/{id}', [
    ContactController::class, 'destroy'
])->name('contact.destroy');