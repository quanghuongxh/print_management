import 'alpinejs';
import Vue from 'vue';

window.$ = window.jQuery = require('jquery');

window.Swal = require('sweetalert2');

//select 2
require('select2');
//Jquery validation
require('jquery-validation');
//bootstrap-datepicker
require('bootstrap-datepicker');
// CoreUI
require('@coreui/coreui');

// Boilerplate
require('../plugins');  

const app = new Vue({
    el: '#app',
});
