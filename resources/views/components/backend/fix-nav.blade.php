@props([
    'leftLink',
    'leftLabel',
    'rightType' => 'button',
    'rightIcon' => 'fa fa-plus',
    'rightLabel',
    'rightLink',
])

<div class="row fix-nav">
    <div class="col-md-6">
        <div class="left-link">
            <i class="fa fa-angle-left" aria-hidden="true"></i>
            <a href="{{ $leftLink }}"> {{ $leftLabel}}</a>
        </div>
    </div>
    <div class="col-md-6">
        <div class="right-button">
            @if ($rightType == 'button') 
                <button id="btnSubmitTrigger" class="btn btn-info"}}> 
                    <i class="{{ $rightIcon }}" aria-hidden="true"></i>
                    {{  $rightLabel }} 
                </button>
            @else
            <a href="{{ $rightLink }}" class="btn btn-info">
                    <i class="fa fa-plus" aria-hidden="true"></i>
                    {{ $rightLabel }}</a> 
            @endif
        </div>
    </div>
</div>
