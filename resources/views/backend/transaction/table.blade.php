<div class="row table-filter">
    <div class="col-md-4">
        <div class="input-group input-daterange" style="float: right">
            <input type="text" class="form-control" placeholder="Ngày bắt đầu" name="startDate" value="">
            <div class="input-group-prepend">
                <span class="input-group-text"> đến </span>
            </div>
            <input type="text" class="form-control" placeholder="Ngày kết thúc" name="endDate" value="">
        </div> 
    </div>
</div>
<table class="table table-hover table-transactions">
    <tr>
        <th>Nguồn</th>
        <th>Giá trị</th>
        <th>Nhân viên tiếp nhận</th>
        <th>Ngày tạo</th>
    </tr>
    @foreach ($transactions as $transaction)
        <tr>
            <td>{{ $transaction->source }}</td>
            <td>{{ number_format($transaction->value, 0) }}</td>
            <td>{{ $transaction->staff }}</td>
            <td>{{ $transaction->created_at}}</td>
        </tr>
    @endforeach
</table>
<div class="table-paginate" style="padding-left:10px;">
    <div class="row">
        <div class="col-md-4">
            {{ $transactions->appends(['customer_id' => $customer_id])->links() }}
        </div>
        <div class="col-md-8">
            <div class="paginate-info">
                Hiển thị {{ ($transactions->currentPage() - 1) *  $transactions->perPage() + 1}} 
                Đến {{ ($transactions->currentPage() - 1) *  $transactions->perPage() + $transactions->count() }} 
                trên {{ $transactions->total() }} kết quả
            </div>
        </div>
        
    </div>
</div>