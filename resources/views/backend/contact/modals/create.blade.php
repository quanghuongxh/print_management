<form class="form" id="modalForm">
    <input type="hidden" value="{{ $customer }}" name="customer">
    <div class="form-group">
        <label for="txtName">{{ __('Họ tên') }} <span style="color:red">*</span></label>
        <input type="text" class="form-control" id="txtName" placeholder="Nhập họ tên" name="name" value="">
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="txtPhone">{{ __('Số điện thoại') }}</label>
            <input type="text" class="form-control" id="txtPhone" name="phone" placeholder="Nhập điện thoại" value="">
        </div>
        <div class="form-group col-md-6">
            <label for="txtZalo">{{ __('Zalo') }}</label>
            <input type="text" class="form-control" id="txtZalo" name="zalo" placeholder="Nhập Zalo" value="">
        </div>
    </div>

    <div class="form-group">
        <label for="txtEmail">{{ __('Email') }}</label>
        <input type="email" class="form-control" id="txtEmail" placeholder="Nhập email" name="email" value="">
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="txtDepartment">{{ __('Phòng/ban') }}</label>
            <input type="text" class="form-control" id="txtDepartment" name="department" placeholder="Nhập phòng/ban" value="">
        </div>
        <div class="form-group col-md-6">
            <label for="txtPosition">{{ __('Chức vụ') }}</label>
            <input type="text" class="form-control" id="txtPosition" name="position" placeholder="Nhập chức vụ" value="">
        </div>
    </div>

    <div class="form-group">
        <label for="txtNote">{{ __('Ghi chú') }}</label>
        <textarea class="form-control" name="note" id="txtNote"  rows="3"></textarea>
    </div>
    <div class="form-row">
        <div class="col-md-12 pull-right">
            <button type="button" class="btn btn-warning" data-dismiss="modal">{{ __('Close') }}</button>
            <button class="btn btn-info" id="btnContactCreate" data-href="{{ route('admin.contact.store') }}">{{ __('Save contact') }}</button>
        </div>
    </div>
</form>