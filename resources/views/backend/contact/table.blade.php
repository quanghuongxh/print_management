<table class="table table-hover table-contacts">
    <tr>
        <th>{{ __('Tên') }}</th>
        <th>{{ __('Số Điện Thoại') }}</th>
        <th>{{ __('Zalo') }}</th>
        <th>{{ __('Email') }}</th>
        <th>{{ __('Action') }}</th>
    </tr>
    @foreach ($contacts as $contact)
        <tr>
            <td>{{ $contact->name }}</td>
            <td>{{ $contact->phone }}</td>
            <td>{{ $contact->zalo }}</td>
            <td>{{ $contact->email }}</td>
            <td>
                <a href="{{ route('admin.contact.edit', $contact) }}" class="btn btn-info btn-sm btn-contact-edit" >  
                    <i class="fas fa-pencil-alt"></i> {{ __('Edit') }}
                </a>
                <a href="{{ route('admin.contact.destroy', $contact->id) }}" class="btn btn-danger btn-sm btn-contact-delete">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                    {{ __('Delete') }}
                </a>
            </td>
        </tr>
    @endforeach
</table>

<div class="row table-footer">
    <div class="col-md-12">
        <div class="table-footer-right">
            <x-utils.link class="link-btn" id="addContactBtn" icon="fa fa-plus" :href="route('admin.contact.create', $customer_id)" :text="__('Add new contact')" />
        </div>
    </div>
</div>
