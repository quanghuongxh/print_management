<div class="row fix-nav">
    <div class="col-md-6">
        <div class="left-link">
            <i class="fa fa-angle-double-left" aria-hidden="true"></i>
            <a href="{{ $link ?? '#' }}"> {{ $link_title ?? 'Trang chủ' }}</a>
        </div>
    </div>
    <div class="col-md-6">
        <div class="right-button">
            <button id="btnSubmitTrigger" class="btn btn-info"}}> {{ $button_label ?? 'Lưu thông tin' }} </button>
        </div>
    </div>
</div>
