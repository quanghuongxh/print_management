<x-livewire-tables::bs4.table.cell>
    {{ $row->id }}
</x-livewire-tables::bs4.table.cell>

<x-livewire-tables::bs4.table.cell>
    {{ $row->name }}
</x-livewire-tables::bs4.table.cell>

<x-livewire-tables::bs4.table.cell>
    @if($row->status) <span class="badge badge-success" >{{ config('xuanhoa.customer.status')[$row->status] }}</span> 
    @else
        <span class="badge badge-warning" >{{ config('xuanhoa.customer.status')[$row->status] }}</span> 
    @endif
</x-livewire-tables::bs4.table.cell>

<x-livewire-tables::bs4.table.cell>
   {{ $row->group_label }}
</x-livewire-tables::bs4.table.cell>

<x-livewire-tables::bs4.table.cell>
    {{ $row->hotline }}
</x-livewire-tables::bs4.table.cell>

<x-livewire-tables::bs4.table.cell>
    {{ $row->zalo }}
</x-livewire-tables::bs4.table.cell>

<x-livewire-tables::bs4.table.cell>
    {{ config('xuanhoa.customer.type')[$row->type] }}
</x-livewire-tables::bs4.table.cell>

<x-livewire-tables::bs4.table.cell>
    <x-utils.view-button :href="route('admin.customer.show', $row)" />
    <x-utils.edit-button :href="route('admin.customer.edit', $row)" />
    {{-- <x-utils.delete-button :href="route('admin.customer.destroy', $row)" /> --}}
</x-livewire-tables::bs4.table.cell>
