@extends('backend.layouts.app')

@section('title', __('Customer Management'))

@section('xh-form-button')
    <div class="row fix-nav">
        <div class="col-md-6">
            <div class="left-link">
                <i class="fa fa-angle-left" aria-hidden="true"></i>
                <a href="{{ route('admin.customer.index') }}"> {{ __('Danh sách khách hàng') }}</a>
            </div>
        </div>
        <div class="col-md-6">
            <div class="right-button">
                <div class="dropdown" style="float: left">
                    <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown">
                        {{ __('Tạo phiếu thu/chi')}}
                    <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                      <li><a href="#">Tạo phiếu thu</a></li>
                      <li><a href="#">Tạo phiếu chi</a></li>
                    </ul>
                </div>
                <x-utils.delete-button :href="route('admin.customer.destroy', $customer)" />
            </div>
        </div>
    </div>
@endsection

@section('content')
    <h3 class="customer-name"><span style="font-weight: 400;"> {{ __('Khách hàng: ')  }}</span> {{ $customer->name }}</h3>
    <div class="row customer-detail">
        <div class="col-md-8">
            <x-backend.card>
                <x-slot name="header">
                    {{ __('Thông tin cá nhân')}} - 
                    <span class="badge {{ $customer->status ? 'badge-success' : 'badge-warning' }}">
                        {{ config('xuanhoa.customer.status')[$customer->status] }} 
                    </span>
                </x-slot>
        
                <x-slot name="headerActions">
                    <x-utils.link
                        class="card-header-action"
                        :href="route('admin.customer.edit', $customer)"
                        :text="__('Cập nhật')"
                    />
                </x-slot>
        
                <x-slot name="body">
                    <div class="row xh-table">
                        <div class="col-md-7">
                            <table class="table table-condensed">
                                <tr>
                                    <td width="30%">ID</td>
                                    <td class="value">: {{ $customer->code }}</td>
                                </tr>
                                <tr>
                                    <td>Hotline</td>
                                    <td class="value">: {{ $customer->hotline ?? '--' }}</td>
                                </tr>
                                <tr>
                                    <td>Điện thoại</td>
                                    <td class="value">: {{ $customer->phone ?? '--' }}</td>
                                </tr>
                                <tr>
                                    <td>Zalo</td>
                                    <td class="value">: {{ $customer->zalo ?? '--' }}</td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td class="value">: {{ $customer->email ?? '--' }}</td>
                                </tr>
                              
                                <tr>
                                    <td>Địa chỉ</td>
                                    <td class="value">: {{ $customer->address ?? '--' }}</td>
                                </tr>
                              
                            </table>
                        </div>

                        <div class="col-md-5">
                            <table class="table table-condensed">
                               
                                <tr>
                                    <td>Nhóm</td>
                                    <td class="value">: {{ $customer->group_label ?? '--' }}</td>
                                </tr>
                                <tr>
                                    <td>Loại</td>
                                    <td class="value">: {{ config('xuanhoa.customer.type')[$customer->type] }}</td>
                                </tr>
                                @if ($customer->type)
                                    <tr>
                                        <td>Công ty</td>
                                        <td class="value">: {{ $customer->company ?? '--' }}</td>
                                    </tr>
                                    <tr>
                                        <td>Mã số Thuế</td>
                                        <td class="value">: {{ $customer->tax_code ?? '--' }}</td>
                                    </tr>
                                @endif
                            </table>
                        </div>
                    </div>
                </x-slot>
            </x-backend.card>
        </div>

        <div class="col-md-4">
            <x-backend.card>
                <x-slot name="header">
                    {{ __('Thông tin đặt in') }}
                </x-slot>
        
                <x-slot name="headerActions">
                    
                </x-slot>
        
                <x-slot name="body">
                    <div class="row xh-table">
                        <div class="col-md-12">
                            <table class="table table-condensed">
                                <tr>
                                    <td>{{ __('Tổng chi tiêu') }}</td>
                                    <td class="value">: 93.000.000 vnđ </td>
                                </tr>

                                <tr>
                                    <td>{{ __('SL đơn hàng') }}</td>
                                    <td class="value">: 201 </td>
                                </tr>

                                <tr>
                                    <td>{{ __('Ngày in gần nhất') }}</td>
                                    <td class="value">: 03/08/2021 </td>
                                </tr>

                                <tr>
                                    <td>{{ __('Công nợ hiện tại') }}</td>
                                    <td class="value">: {{ number_format($customer->transactions->sum('value'), 0) }} vnđ</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </x-slot>
            </x-backend.card>
        </div>

    </div>
    <!-- tab content -->
    <div class="row customer-tab-content">
        <div class="col-md-12 ml-auto col-xl-12 mr-auto">
          <!-- Nav tabs -->
          <div class="card">
            <div class="card-header">
              <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" data-toggle="tab" href="#ordersTab" role="tab">
                   {{ __('Lịch sử đặt in') }}
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#transactionsTab" role="tab">
                    {{ __('Công nợ') }}
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#contactsTab" role="tab">
                    {{ __('Thông tin liên hệ') }}
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#notesTab" role="tab">
                    {{ __('Ghi chú') }}
                  </a>
                </li>
              </ul>
            </div>
            <div class="card-body">
              <!-- Tab panes -->
              <div class="tab-content">
                <div class="tab-pane active" id="ordersTab" role="tabpanel">
                    
                </div>
                <div class="tab-pane " id="transactionsTab" role="tabpanel">
                   @include('backend.transaction.table', ['transactions' => $transactions, 'customer_id' => $customer->id])
                </div>
                <div class="tab-pane" id="contactsTab" role="tabpanel">
                    @include('backend.contact.table', ['contacts' => $customer->contacts, 'customer_id' => $customer->id])
                </div>
                <div class="tab-pane" id="notesTab" role="tabpanel">
                    <table class="table table-hover" id="table-notes">
                        
                    </table>
                </div>
                @include('includes.partials.loading-spinner')
              </div>
            </div>

          </div>
        </div>
    </div>
    <!-- /end tab -->
@endsection

<x-utils.modal title="Nhập thông tin liên hệ"/>

@push('after-scripts')
    <script>
        $(document).ready(function(){
            /* table ajax with pagination */
            $('body').on('click', '#transactionsTab .pagination a', function(e) {
                e.preventDefault();
                let url = $(this).attr('href'); 
                $('.loading-spiner').show();
            
                $.ajax({
                    url : url  
                }).done(function (data) {
                    $('#transactionsTab').html(data);  
                    $('.loading-spiner').hide();
                }).fail(function () {
                    console.log('Load transactions fail');
                    $('.loading-spiner').hide();
                });
                
            });

            /** datetimepicker - move to global */
            $('.input-daterange input').each(function() {
                $(this).datepicker({
                    'clearDates' : true,
                    'todayHighlight': true,
                    'language': 'vi',
                    'endDate': '0d'
                });
            });

            // display a modal (medium modal)
            $('body').on('click', '.btn-contact-edit', function(e) {
                event.preventDefault();
                let href = $(this).attr('href');

                $.ajax({
                    url: href,
                    beforeSend: function() {
                        $('#loader').show();
                    },
                    // return the result
                    success: function(result) {
                        $('#mediumModal').modal("show");
                        $('#mediumBody').html(result).show();
                    },
                    complete: function() {
                        $('#loader').hide();
                    },
                    error: function(jqXHR, testStatus, error) {
                        console.log(error);
                        alert("Page " + href + " cannot open. Error:" + error);
                        $('#loader').hide();
                    },
                    timeout: 8000
                });

            });// end btn-contact-edit

            /** update contact modal ajax */
            $('body').on('click', '#btnContactUpdate', function(e) {
                e.preventDefault();
                let $this = $(this);
                let url = $this.attr('data-href');

                let formData = {
                    name: $('#txtName').val(),
                    phone: $('#txtPhone').val(),
                    zalo: $("#txtZalo").val(),
                    email: $('#txtEmail').val(),
                    department: $('#txtDepartment').val(),
                    position: $('#txtPosition').val(),
                    note: $('#txtNote').val()
                };

                $('.loading-spiner').show();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: url,
                    method:"PUT",
                    data:formData,
                    success:function(data) {
                        $('#mediumModal').modal("toggle");
                        $('#mediumBody').html('<div> </div>');
                        $('#contactsTab').html(data); 
                        $('.loading-spiner').hide();
                    },
                    error: function(data) {
                        console.log('error!');
                    }
                });

            });
            /* end */

            $('body').on('click', '#addContactBtn', function(e) {
                event.preventDefault();
                let href = $(this).attr('href');
                $.ajax({
                    url: href,
                    
                    // return the result
                    success: function(result) {
                        $('#mediumModal').modal("show");
                        $('#mediumBody').html(result).show();
                    },
                    error: function(jqXHR, testStatus, error) {
                        console.log(error);
                        alert("Page " + href + " cannot open. Error:" + error);
                        $('#loader').hide();
                    },
                    timeout: 8000
                });

            });//end  addContactBtn click

            $('body').on('click', '#btnContactCreate', function(e) {
                e.preventDefault();
                let $this = $(this);
                let url = $this.attr('data-href');

                let formData = {
                    name: $('#txtName').val(),
                    phone: $('#txtPhone').val(),
                    zalo: $("#txtZalo").val(),
                    email: $('#txtEmail').val(),
                    department: $('#txtDepartment').val(),
                    position: $('#txtPosition').val(),
                    note: $('#txtNote').val(),
                    customer: $('input[name=customer]').val()
                };

                $('.loading-spiner').show();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    url: url,
                    method:"POST",
                    data:formData,
                    success:function(data) {
                        $('#mediumModal').modal("toggle");
                        $('#mediumBody').html('<div> </div>');
                        $('#contactsTab').html(data); 
                        $('.loading-spiner').hide();
                    },
                    error: function(data) {
                        console.log('error!');
                    }
                });
                
            }); //end create contact form ajax

            $('body').on('click', '.btn-contact-delete', function(e) {
                event.preventDefault();
                let href = $(this).attr('href');

                Swal.fire({
                    title: 'Bạn muốn xoá liên hệ?',
                    showCancelButton: true,
                    confirmButtonText: 'Xác nhận xoá',
                    cancelButtonText: 'Huỷ',
                    icon: 'warning'
                }).then((result) => {
                    if (result.value) {
                        ajax_delete_contact(href);
                    } 
                }); 

            }); //end btn-contact-delete
            
        });//end ready

        /**************** function *********/
        function ajax_delete_contact(url) {
            $('.loading-spiner').hide();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: url,
                method:"DELETE",
                success:function(data) {
                    $('#contactsTab').html(data); 
                    $('.loading-spiner').hide();
                },
                error: function(data) {
                    console.log('error!');
                }
            });
        } //end function delete contact ajax

    </script>
@endpush
