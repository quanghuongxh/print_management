@extends('backend.layouts.app')

@section('title', __('Customer Group Management'))

@section('xh-form-button')
    <x-backend.fix-nav 
        leftLink="{{ route('admin.dashboard') }}" 
        leftLabel="{{ __('Dashboard') }}"
        rightType="link"
        rightLink="{{ route('admin.customer-group.create') }}"
        rightLabel="{{ __('Thêm nhóm mới') }}"
    />
@endsection


@section('content')
    <x-backend.card>

        <x-slot name="header">
            @lang('Customer Group Management')
        </x-slot>

        <x-slot name="headerActions">
          
        </x-slot>

        <x-slot name="body">
            <livewire:backend.customer-group-table />
        </x-slot>
    </x-backend.card>
@endsection
