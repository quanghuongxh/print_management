@extends('backend.layouts.app')

@section('title', __('Edit Customer Group'))


@section('xh-form-button')
    <x-backend.fix-nav 
        leftLink="{{ route('admin.customer-group.index') }}" 
        leftLabel="{{ __('Danh sách nhóm') }}"
        rightLabel="{{ __('Cập nhật nhóm') }}"
    />
@endsection

@section('content')
    <x-forms.patch :action="route('admin.customer-group.update', $customer_group)" id="submitForm">
        <div class="row customer-group-edit-page">
            <div class="col-md-12">
                <x-backend.card>
                    <x-slot name="header">
                        {{ __('Thông tin nhóm khách hàng')}}
                    </x-slot>

                    <x-slot name="headerActions">
                        <x-utils.link
                            icon="c-icon cil-plus"
                            class="card-header-action"
                            :href="route('admin.customer-group.create')"
                            :text="__('Create Customer Group')"
                        />
                    </x-slot>

                    <x-slot name="body">
                        <!-- start table customer -->
                        <div class="form-row">
                            <div class="col-md-6 form-group">
                                <label for="inputName">{{ __('Customer name') }}</label>
                                <input value="{{ $customer_group->name }}" type="text" class="form-control" id="inputName" name="name" placeholder="{{ __('Full name') }}">
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="inputCode">{{ __('Customer ID') }}</label>
                                <input type="text" value="{{ $customer_group->code }}" class="form-control" id="inputCode" name="code" placeholder="{{ __('ID') }}">
                            </div>
                            <div class="col-md-12 form-group" style="padding-bottom: 20px;">
                                <label for="txtDescription">{{ __('Description') }}</label>
                                <textarea  class="form-control" id="txtDescription" rows="2" name="description">{{ $customer_group->description }}</textarea>
                            </div>
                           
                            <div class="col-md-6 form-group">
                                <label for="selectPrice">{{ __('Giá áp dụng') }}</label>
                                <select name="price" class="select2 form-control" id="selectPrice">
                                    <option value="1">In khách lẻ</option>
                                    <option value="2">Khách in thuờng xuyên</option>
                                    <option value="3">Khách mối</option>
                                    <option value="4">Khách công ty</option>
                                </select>
                            </div>

                            <div class="col-md-6 form-group">
                                <label for="txtDiscount">{{ __('Triết khấu ') }} (%)</label>
                                <input class="form-control" type="number" name="discount" id="txtDiscount" placeholder="0">
                            </div>
                            
                            
                        </div>
                        
                    </x-slot>

                </x-backend.card>
            </div>
        </div>
    </x-forms.post>
@endsection