@extends('backend.layouts.app')

@section('title', __('Customer Management'))

<!-- fix breadcrums -->
@section('xh-form-button')
    <x-backend.fix-nav 
        leftLink="{{ route('admin.dashboard') }}" 
        leftLabel="{{ __('Dashboard') }}"
        rightType="link"
        rightLink="{{ route('admin.customer.create') }}"
        rightLabel="{{ __('Thêm khách hàng') }}"
    />
@endsection

@section('content')
    <x-backend.card>

        <x-slot name="header">
            @lang('Danh sách khách hàng')
        </x-slot>

        <x-slot name="body">
            <livewire:backend.customer-table />
        </x-slot>
    </x-backend.card>
@endsection
