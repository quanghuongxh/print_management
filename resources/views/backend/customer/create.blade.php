@extends('backend.layouts.app')

@section('title', __('Create Customer'))

@section('xh-form-button')
    <x-backend.fix-nav 
        leftLink="{{ route('admin.customer.index') }}" 
        leftLabel="{{ __('Danh sách khách hàng') }}"
        rightLabel="{{ __('Lưu khách hàng') }}"
    />
@endsection

@section('content')
    <x-forms.post :action="route('admin.customer.store')" id="submitForm">
        <div class="row customer-page">
            <div class="col-md-12">
                <x-backend.card>
                    <x-slot name="header">
                        @lang('Thông tin khách hàng')
                    </x-slot>

                    <x-slot name="headerActions">
                        <x-utils.link
                            icon="fa fa-user-circle"
                            class="card-header-action"
                            :href="route('admin.customer.index')"
                            :text="__('Back All Customers')"
                        />
                    </x-slot>

                    <x-slot name="body">
                        <div class="row customer-form">
                            <div class="col-md-8 customer-form-left">

                                <div class="form-group">
                                    <label for="txtName">{{ __('Tên khách hàng') }}<span style="color:red">*</span></label>
                                    <input type="text" name="name" id="txtName" class="form-control" value="{{ request()->old('name') }}" placeholder="Nhập tên khách hàng">
                                </div>
                                
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <label for="txtHotline">{{ __('Hotline') }}</label>
                                        <input type="text" name="hotline" id="txtHotline" class="form-control" value="{{ request()->old('hotline') }}" placeholder="Nhập số hotline">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="txtPhone">{{ __('Số điện thoại') }}</label>
                                        <input type="text" name="phone" id="txtPhone" class="form-control" value="{{ request()->old('phone') }}" placeholder="Nhập số điện thoại khác">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <label for="txtEmail">{{ __('Email') }}</label>
                                        <input type="mail" name="email" id="txtEmail" class="form-control" value="{{ request()->old('email') }}" placeholder="Nhập Email">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label for="txtZalo">{{ __('Zalo') }}</label>
                                        <input type="text" name="zalo" id="txtZalo" class="form-control" value="{{ request()->old('zalo') }}" placeholder="Nhập Zalo/tên Zalo">
                                    </div>
                                </div>
                               
                                <div class="form-group">
                                    <label for="txtAddress">{{ __('Địa chỉ') }}</label>
                                    <input type="text" name="address" id="txtAddress" class="form-control" value="{{ request()->old('address') }}" placeholder="Nhập địa chỉ">
                                </div>
                            
                            </div>

                            <div class="col-md-4 customer-form-right">
                                <div class="form-group">
                                    <label for="selectGroup">Nhóm khách hàng</label>
                                    <select name="group_id" id="selectGroup" class="form-control select2">
                                        @foreach ($customer_groups as $group)
                                            <option value="{{ $group->id }}"> {{ $group->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                
                                
                               <div class="customer-company">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="type" id="radioType1" value="0" checked>
                                        <label class="form-check-label" for="radioType1">Cá nhân</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="type" id="radioType2" value="1">
                                        <label class="form-check-label" for="radioType2">Công ty</label>
                                    </div>
                               </div>

                                <div class="company-info">
                                    <div class="form-group">
                                        <label for="txtCompany">Tên công ty</label>
                                        <input type="text" name="company" id="txtCompany" class="form-control" placeholder="Nhập tên công ty">
                                    </div>
                                    <div class="form-group">
                                        <label for="txtTaxCode">Mã số thuế</label>
                                        <input type="text" name="tax_code" id="txtTaxCode" class="form-control" placeholder="Nhập mã số thuế">
                                    </div>
                                </div>

                                <div class="customer-company">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="status" id="radioStatus2" value="1" checked> 
                                        <label class="form-check-label" for="radioStatus2">Đã giao dịch</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="status" id="radioStatus1" value="0">
                                        <label class="form-check-label" for="radioStatus1">Chưa giao dịch</label>
                                    </div>
                               </div>

                               <div class="form-group">
                                   <label for="txtDescription">Ghi chú</label>
                                   <textarea name="description" id="txtDescription" class="form-control" rows="5"></textarea>
                               </div>
                            </div>
                        </div>
                    </x-slot>

                </x-backend.card>
            </div>
        </div>
    </x-forms.post>
@endsection

@push('after-scripts')
    <script>
       $(document).ready(function() {
            $('.company-info').hide();
            $('input[type=radio][name=type]').change(function() {
                if (this.value == '0') {
                    $('.company-info').hide();
                }
                else if (this.value == '1') {
                    $('.company-info').slideDown();
                }
            });

            $("#myform").validate({
            rules: {
                // simple rule, converted to {required:true}
                name: "required",
                // compound rule
                email: {
                required: true,
                email: true
                }
            }
            });
       });
    </script>
@endpush