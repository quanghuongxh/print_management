<?php

return [
    'customer' => [
        'type' => ['Cá Nhân', 'Công Ty'],
        'status' => ['Chưa giao dịch', 'Đang giao dịch'],
        'customer_prefix' => 'KH000',
    ]
];
