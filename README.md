## Config laradock > .env
APP_CODE_PATH_HOST=duong_dan_toi_thu_muc_code trên máy thật 

## run docker: cd tới laradock
chạy: docker-compose up -d nginx mariadb

## Connect toi phan mềm quản trị CSDL (ex: Sequel Pro, Mysql Workband...) với thông tin
host: 127.0.0.1
username: root
password: root

Tạo db ten xh_management

## them domail vào thư /etc/hosts
127.0.0.1   momain


## cd tới folder laradock: exec vào terminal của docker nginx
docker-compose exec workspace bash

# run terminal
composer install
npm install
php artisan key:generate
php artisan migrate
php artisan db:seed

npm run dev
php artisan storage:link

login; admin@inxuanhoa.com pass: dinhhuong

