<?php

namespace App\Http\Requests\Backend\Customer;

use Illuminate\Foundation\Http\FormRequest;

class CreateCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|min:3',
            'email'         => 'nullable|email',
            'hotline'       => 'nullable|min:9',
            'phone'         => 'nullable|min:9',
            'address'       => 'nullable|min:20',
            'type'          => 'in:0,1',
            'status'        => 'in:0,1',
        ];
    }
}
