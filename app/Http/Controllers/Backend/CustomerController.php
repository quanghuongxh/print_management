<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\CustomerService;
use App\Services\CustomerGroupService;
use App\Services\TransactionService;
use App\Services\ContactService;
use App\Http\Requests\Backend\Customer\CreateCustomerRequest;

class CustomerController extends Controller
{    
    /**
     * customerService logic
     *
     * @var mixed
     */
    protected $customerService;
    
    /**
     * customerGroupService
     *
     * @var mixed
     */
    protected $customerGroupService;
    
    /**
     * transactionService
     *
     * @var mixed
     */
    protected $transactionService;
    
        
    /**
     * contactService
     *
     * @var mixed
     */
    protected $contactService;
    
    /**
     * __construct 
     *
     * @param  mixed $service CustomerService
     * @return void
     */
    public function __construct(CustomerService $customer_service, 
                                CustomerGroupService $customer_group_service,
                                TransactionService $transaction,
                                ContactService $contact)
    {
        $this->customerService = $customer_service;
        $this->customerGroupService = $customer_group_service;
        $this->transactionService = $transaction;
        $this->contactService = $contact;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.customer.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customer_groups = $this->customerGroupService->all('id', 'name');
        return view('backend.customer.create', ['customer_groups' => $customer_groups]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCustomerRequest $request)
    {
        $this->customerService->create($request->input());
        return redirect()->route('admin.customer.index')->withFlashSuccess(__('Thêm mới khách hàng thành công'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = $this->customerService
                        ->with('contacts')
                        ->getById($id);

        $transactions = $this->transactionService
                            ->where('customer_id', $id)
                            ->paginate(5, ['*'], 'p_trans');
        $transactions->withPath('transaction/get');

        return view('backend.customer.show', [
                    'customer' => $customer, 
                    'transactions' => $transactions
                ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = $this->customerService->getById($id);
        $customer_groups = $this->customerGroupService->all()->pluck('name','id');
        return view('backend.customer.edit', ['customer' => $customer, 'customer_groups' => $customer_groups]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = $this->customerService->update($id, $request->input());
        return redirect()->route('admin.customer.show', $customer)->withFlashSuccess(__('Cập nhật khách hàng thành công'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->customerService->destroy($id);
        return redirect()->route('admin.customer.index')->withFlashSuccess(__('Đã xoá thành công khách hàng'));
    }

    /******************************* For load ajax transaction ********************************* */
    public function loadTransaction(Request $request)
    {
        $transactions = $this->transactionService->where('customer_id', $request->customer_id)
                                                ->paginate(5, ['*'], 'p_trans');
        if ($request->ajax()) {
            return view('backend.transaction.table', ['transactions' => $transactions, 
                                                'customer_id' => $request->customer_id])
                                                ->render();  
        }
        return redirect()->route('admin.customer.index');
    }
}
