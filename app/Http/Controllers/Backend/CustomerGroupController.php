<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\CustomerGroupService;
use App\Http\Requests\Backend\CustomerGroup\CreateCustomerGroupRequest;
use App\Http\Requests\Backend\CustomerGroup\UpdateCustomerGroupRequest;


class CustomerGroupController extends Controller
{    
    /**
     * customerGroupService store service for data
     *
     * @var mixed
     */
    protected $customerGroupService;
    
    /**
     * __construct
     *
     * @param  mixed $customerGroupService
     * @return void
     */
    public function __construct(CustomerGroupService $customerGroupService)
    {
        $this->customerGroupService = $customerGroupService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.customer.group.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.customer.group.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCustomerGroupRequest $request)
    {
        $group = $this->customerGroupService->store($request->input());
        return redirect()->route('admin.customer-group.index')->withFlashSuccess(__('Tạo nhóm khách hàng thành công'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer_group = $this->customerGroupService->getById($id);
        return view('backend.customer.group.edit', ['customer_group' => $customer_group]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCustomerGroupRequest $request, $id)
    {
        $group = $this->customerGroupService->update($id, $request->input());
        return redirect()->route('admin.customer-group.index')->withFlashSuccess(__('Cập nhật nhóm khách hàng thành công'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $group = $this->customerGroupService->getById($id);
        if ($group)  {
            $this->customerGroupService->destroy($group);
            return redirect()->back()->withFlashSuccess(__('Xoá dữ liệu thành công'));
        } else {
            return redirect()->back()->withFlashError(__('Không tìm thấy nhóm cần xoá'));
        }
        
    }
}
