<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Services\ContactService;
use Illuminate\Http\Request;

class ContactController extends Controller
{    
    /**
     * contactService
     *
     * @var mixed
     */
    protected $contactService;
    
    /**
     * __construct
     *
     * @param  mixed $contact
     * @return void
     */
    public function __construct(ContactService $contact)
    {
        $this->contactService = $contact;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($customer)
    {
        if (request()->ajax()) {
            return response()->view('backend.contact.modals.create', ['customer' => $customer], 200)
                            ->header('Content-Type', 'text/plain');   
        }

        return redirect()->route('admin.customer.index'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contact = $this->contactService->create($request->input());
        $customer = $contact->customer;
        $contacts = $customer->contacts;
        
        return response()->view('backend.contact.table', ['contacts' => $contacts, 'customer_id' => $customer->id], 200)
                            ->header('Content-Type', 'text/plain');  
     
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        $contact = $this->contactService->getById($id);

        if ($request->ajax()) {
            return view('backend.contact.modals.edit', ['contact' => $contact])->render();  
        }

        return view('backend.contact.modals.edit', ['contact' => $contact])->render();  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $contact = $this->contactService->update($request->all(),$id);
        $customer = $contact->customer;
        $contacts = $customer->contacts;

        if ($request->ajax()) {
            return response()->view('backend.contact.table', ['contacts' => $contacts, 'customer_id' => $customer->id], 200)
                            ->header('Content-Type', 'text/plain');  
        }
        
        return redirect()->route('admin.customer.index'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact = $this->contactService->getById($id);
        $customer = $contact->customer;
        
        if ($this->contactService->deleteById($id)) {
            $contacts = $customer->contacts;
            return response()->view('backend.contact.table', ['contacts' => $contacts, 'customer_id' => $customer->id], 200);
        } else {
            $contacts = $customer->contacts;
            return response()->view('backend.contact.table', ['contacts' => $contacts, 'customer_id' => $customer->id], 400);
        }
    }
}
