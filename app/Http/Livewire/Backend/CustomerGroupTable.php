<?php

namespace App\Http\Livewire\Backend;

use App\Models\CustomerGroup;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\Views\Filter;


class CustomerGroupTable extends DataTableComponent
{    
    /**
     * Khoi tao bang cho livewire
     *
     * @return array
     */
    public function columns(): array
    {
        return [
            Column::make('ID'),
            Column::make('Code')
                ->sortable()
                ->searchable(),
            Column::make('Name')
                ->sortable()
                ->searchable(),
            Column::make('Created', 'updated_at')
                ->sortable(),
            Column::make('Actions'),
        ];
    }
    
    /**
     * basic query : thuc hien cac thao tac du lieu
     *
     * @return Builder
     */
    public function query(): Builder
    {
        return CustomerGroup::query();
    }
    
    
    /**
     * render table 
     *
     * @return string
     */
    public function rowView(): string
    {
        return 'backend.customer.group.row';
    }
}
