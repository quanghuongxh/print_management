<?php

namespace App\Http\Livewire\Backend;

use Livewire\Component;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\Views\Filter;
use App\Models\Customer;
use App\Models\CustomerGroup;

class CustomerTable extends DataTableComponent
{
    /**
     * filters 
     *
     * @return array
     */
    public function filters(): array
    {
        $all_customer_groups = CustomerGroup::all()->pluck('name', 'id')->toArray();
        $first = 'Tất cả';
        array_unshift($all_customer_groups, $first);
        return [
            'status' => Filter::make('Trạng thái')
                ->select([
                    '' => 'Tất cả',
                    '1' => 'Đang giao dịch',
                    '0' => 'Chưa giao dịch',
                ]),
            'group' => Filter::make('Nhóm khách hàng')
                ->select($all_customer_groups),
        ];
    }
    /**
     * Khoi tao bang cho livewire
     *
     * @return array
     */
    public function columns(): array
    {
        return [
            Column::make('ID', 'id')
                ->sortable(),
            Column::make('Name')
                ->sortable()
                ->searchable(),
            Column::make('Status'),
            Column::make('Group'),
            Column::make('Hotline'),
            Column::make('Zalo')
                ->searchable(),
            Column::make('Type'),
            Column::make('Actions'),
        ];
    }
    
    /**
     * basic query : thuc hien cac thao tac du lieu
     *
     * @return Builder
     */
    public function query(): Builder
    {
        $query = Customer::with('customer_group');

        if ($this->hasFilter('status')) {
            if ($this->getFilter('status') === 1) {
                $query = $query->where('status', 1);
            }
            else {
                $query = $query->where('status',0);
            }
        }
        $query = $query->when($this->getFilter('group'), fn ($query, $group) => $query->where('customer_group_id', $group));
        $query = $query->orderBy('id', 'DESC');

        return $query;
    }
    
    
    /**
     * render table 
     *
     * @return string
     */
    public function rowView(): string
    {
        return 'backend.customer.row';
    }
}
