<?php

namespace App\Models;

use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Customer;

class CustomerGroup extends Model
{
    use HasFactory;
    
    /**
     * table of Customer
     *
     * @var string
     */
    protected $table = 'xh_customer_group';

    protected $fillable = [
        'name',
        'code',
        'description',
    ];
    
     /*********************************************************************************
     *                                     Relationship
     * /******************************************************************************/
     
     public function customers()
     {
         return $this->hasMany(Customer::class);
     }

    /*********************************************************************************
     *                                     methods
     * /******************************************************************************/

     /**
     * Do something where delete group: can not delete id = 1, update all customer with group to id = 1
     *
     * @return void
     */
    public static function boot() {
        parent::boot();

        static::deleting(function($group) { 
            //check default group
            if ($group->id == 1) {
                throw new GeneralException(__('Không thể xoá nhóm mặc định.'));
            }
           
            return true;
        });
    }
    
    /**
     * Check group can be delete if not default (id-1)
     *
     * @return bool
     */
    public function canDelete() : bool
    {
        if ($this->id == 1) {
            return false;
        }
        return true;
    }
   
}
