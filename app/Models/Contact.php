<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use HasFactory;

    protected $table = 'xh_contacts';

    protected $fillable = [
        'customer_id',
        'name',
        'phone',
        'zalo',
        'email',
        'department',
        'position',
        'note',
    ];
    
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}
