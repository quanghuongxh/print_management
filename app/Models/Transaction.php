<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Customer;

class Transaction extends Model
{
    use HasFactory;
    
    /**
     * table
     *
     * @var string
     */
    protected $table = 'xh_transactions';
    
    /**
     * fillable
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'customer_id',
        'value',
        'source',
        'username',
        'note',
    ];
    
    /**
     * relationship many - one with user
     *
     * @return void
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}
