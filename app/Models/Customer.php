<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\CustomerGroup;
use App\Models\Transaction;


class Customer extends Model
{
    use HasFactory;
    
    /**
     * customer table = xh_customers
     *
     * @var string
     */
    protected $table = 'xh_customers';

    protected $fillable = [
        'name',
        'code',
        'status',
        'customer_group_id',
        'hotline',
        'phone',
        'email',
        'zalo',
        'address',
        'type',
        'company',
        'tax_code',
        'description',
    ];
    
    /**
     * All relationship
     */

     
    /**
     * Relation ship to customerGroup
     *
     * @return void
     */
    public function customer_group()
    {
        return $this->belongsTo(CustomerGroup::class);
    }
    
    /**
     * transactions
     *
     * @return void
     */
    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    /**
     * relationship contacts
     */
    public function contacts()
    {
        return $this->hasMany(Contact::class);
    }
    
    /**
     * getGroupLabelAttribute: group_label
     *
     * @return void
     */
    public function getGroupLabelAttribute()
    {
        if ($this->customer_group) {
            return $this->customer_group->name;
        }
        return 'N/A';
    }
}
