<?php

namespace App\Services;

use App\Exceptions\GeneralException;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Models\Customer;


/**
 * Class CustomerService.
 */
class CustomerService extends BaseService
{
    /**
     * CustomerService constructor.
     *
     * @param  Customer  $Customer
     */
    public function __construct(Customer $customer)
    {
        $this->model = $customer;
    }
    
    /**
     * store customer to db
     *
     * @param  mixed $data
     * @return Customer
     */
    public function create(array $data = []) : Customer 
    {
        DB::beginTransaction();

        try {
            $customer = $this->model->create([
                'name'      => $data['name'],
                'code'      => $data['code'] ?? null,
                'status'    => $data['status'],
                'customer_group_id'  => $data['group_id'],
                'hotline'   => $data['hotline'],
                'phone'     => $data['phone'],
                'email'     => $data['email'],
                'zalo'      => $data['zalo'],
                'address'   => $data['address'],
                'type'      => $data['type'],
                'company'   => $data['company'],
                'tax_code'  => $data['tax_code'],
                'description'   => $data['description'],
            ]);

            //cap nhat code
            $customer->code = config('xuanhoa.customer.customer_prefix') . $customer->id;
            $customer->save();
            
        } catch (Exception $e) {
            DB::rollBack();

            throw new GeneralException(__('Có lỗi sảy ra khi thêm khách hàng: ') . $e->getMessage());
        }

        DB::commit();

        return $customer;
    }
    
    /**
     * update customer to db
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return Customer
     */
    public function update($id, array $data = []) : Customer
    {
        DB::beginTransaction();

        try {
            $customer = $this->getById($id);
            $customer->update([
                'name'      => $data['name'],
                'status'    => $data['status'],
                'customer_group_id'  => $data['group_id'],
                'hotline'   => $data['hotline'],
                'phone'     => $data['phone'],
                'email'     => $data['email'],
                'zalo'      => $data['zalo'],
                'address'   => $data['address'],
                'type'      => $data['type'],
                'company'   => $data['company'],
                'tax_code'  => $data['tax_code'],
                'description'   => $data['description'],
            ]);
        } catch (Exception $e) {
            DB::rollBack();

            throw new GeneralException(__('Có lỗi sảy ra khi cập nhật khách hàng: ') . $e->getMessage());
        }

        DB::commit();

        return $customer;
    }
    
    /**
     * destroy customer by ID
     *
     * @param  mixed $id
     * @return bool
     */
    public function destroy($id) : bool
    {
        if ($this->deleteById($id)) {
            return true;
        }

        throw new GeneralException(__('Có lỗi sảy ra khi xoá khách hàng: ') . $e->getMessage());
    }

}
