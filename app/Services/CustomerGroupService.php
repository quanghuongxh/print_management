<?php

namespace App\Services;

use App\Exceptions\GeneralException;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Models\CustomerGroup;


/**
 * CustomerGroupService
 */
class CustomerGroupService extends BaseService
{    
    /**
     * __construct
     *
     * @param  mixed $customerGroup
     * @return void
     */
    public function __construct(CustomerGroup $customerGroup)
    {
        $this->model = $customerGroup;
    }
    
    /**
     * update customer group to db
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return CustomerGroup
     */
    public function update($id, array $data = []) : CustomerGroup
    {
        $group = $this->getById($id);
        
        DB::beginTransaction();

        try {
            $group['name'] = $data['name'];
            $group['code'] = $data['code'];
            $group['description'] = $data['description'] ?? null;

            $group->save();
        } catch (Exception $e) {
            DB::rollBack();
            
            //$e->getMessage()
            throw new GeneralException(__('There was a problem updating the customer group: ') . $e->getMessage());
        }

        DB::commit();

        return $group;
    }
    
    /**
     * Save customer group to db
     *
     * @param  mixed $data
     * @return CustomerGroup
     */
    public function store(array $data = []) : CustomerGroup
    {
        DB::beginTransaction();

        try {
            $group = $this->model->create([
                'name' => $data['name'],
                'code' => $data['code'],
                'description' => $data['description'],
            ]);
        } catch (Exception $e) {
            DB::rollBack();

            throw new GeneralException(__('There was a problem creating the customer group'));
        }

        DB::commit();

        return $group;
    }
    
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy(CustomerGroup $group) :bool
    {
        $customers_with_group = $group->customers();
        if ($this->deleteById($group->id)) {
            //update all customer ralationship to group_id=1 (nogroup)
            $customers_with_group->each(function($customer){
                $customer->update('group_id', 1);
            });

            return true;
        }
    
        throw new GeneralException(__('There was a problem deleting the customer group.'));
    }
}
