<?php

namespace App\Services;

use App\Exceptions\GeneralException;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Models\Transaction;

/**
 * Class TransactionService.
 */
class TransactionService extends BaseService
{
       
    /**
     * __construct
     *
     * @param  mixed $transaction
     * @return void
     */
    public function __construct(Transaction $transaction)
    {
        $this->model = $transaction;
    }
}
