<?php

namespace App\Services;

use App\Exceptions\GeneralException;
use App\Services\BaseService;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Models\Contact;

/**
 * Class ContactService.
 */
class ContactService extends BaseService
{
       
    /**
     * __construct
     *
     * @param  mixed $contact
     * @return void
     */
    public function __construct(Contact $contact)
    {
        $this->model = $contact;
    }
    
    /**
     * update
     *
     * @param  mixed $data
     * @param  mixed $id
     * @return Contact
     */
    public function update(array $data = [], $id) : Contact 
    {
        $contact = $this->getById($id);
        
        DB::beginTransaction();

        try {
            $contact['name'] = $data['name'];
            $contact['phone'] = $data['phone'];
            $contact['zalo'] = $data['zalo'];
            $contact['email'] = $data['email'];
            $contact['department'] = $data['department'];
            $contact['position'] = $data['position'];
            $contact['note'] = $data['note'];

            $contact->save();
        } catch (Exception $e) {
            DB::rollBack();
            //$e->getMessage()
            throw new GeneralException(__('Có lỗi sảy ra khi cập nhật sổ liên hệ: ') . $e->getMessage());
        }

        DB::commit();

        return $contact;
    }
    
    /**
     * create
     *
     * @param  mixed $data
     * @return Contact
     */
    public function create(array $data = []) : Contact
    {
        DB::beginTransaction();

        try {
            $contact = $this->model->create([
               'name' => $data['name'],
                'phone' => $data['phone'],
                'zalo' => $data['zalo'],
                'email' => $data['email'],
                'department' => $data['department'],
                'position' => $data['position'],
                'note' => $data['note'],
                'customer_id' => $data['customer'],
            ]);
        } catch (Exception $e) {
            DB::rollBack();

            throw new GeneralException(__('có lỗi sảy ra khi tạo mới sổ liên hệ: ') . $e->getMessage());
        }
        DB::commit();

        return $contact;
    }
}
